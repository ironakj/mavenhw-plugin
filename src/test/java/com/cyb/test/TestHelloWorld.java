package com.cyb.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.cyb.HelloWorld;

public class TestHelloWorld {

	HelloWorld hw;
	
	@Mock
	HttpServletRequest request;
	@Mock
	HttpServletResponse response;
	
	@Before
	public void setUp(){
		hw=new HelloWorld();
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testdoGet() throws ServletException, IOException {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
	    Mockito.when(response.getWriter()).thenReturn(pw);
		hw.doGet(request, response);
		String result = sw.getBuffer().toString().trim();
		assertEquals("Returned: HelloWorld\nServed at: null", result);
	}

	@Test
	public void testGreetHello() {
		assertEquals("HelloWorld", hw.greetHello());
	}

}
